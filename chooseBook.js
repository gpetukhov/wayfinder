"use strict";

var ChooseBookColl = Backbone.Collection.extend({
	url: 'http://minrva-dev.library.illinois.edu/api/catalog/search'  
	//'http://localhost:8080/MinrvaServices/wayfinder/map_data'
});

var ChooseBookView = Backbone.View.extend({
	initialize: function(){
		var temp;
		that = this;
		that.page = 1;
		this.formatIcons = {};

		require(['text!bookChooseT.html'], function(t) {
			that.fullTemplate = t;
        	that.template = _.unescape($(t).find('.direct-import').html());
        	that.render();
    	});
	},
	el: '#_el',
	events: {
		'click #show-btn': 'clickListener',
		'click .item': 'bookClick',
		'click #prev-button': 'prevPage',
		'click #next-button': 'nextPage',
		'click #drip': 'dropdown'
	},
	render: function () {
		console.log("rendering " + chooseBookColl.location);
		/*var data = {
			location:chooseBookColl.locationLabel
		};*/
		//$('#toptitle').html(chooseBookColl.locationLabel);
		var temp = _.template(this.template, {});
		this.$el.html(temp);
		$('.pager').hide();
		$('#nothing').hide();

		var $select = $("#format-selector");
		//$select.append('<option value=""></option>');
		var formats = ["Any format", "Book", "Electronic", "Journal / Magazine", "Microform", "Music Score", "Music Recording", "Map", "Manuscript", "Film or Video", 
		"Sound Recording", "Software / Computer File", "Music Manuscript", "Textual Material", "Archive", "Archival Collection", 
		"2D Art", "Mixed Material", "Kit", "Manuscript Map", "3D Object"];

		for(var i=0; i<formats.length; i++)
		{
			if(formats[i]=="Any format") 
				$select.append('<option value="">' + formats[i] + '</option>');
			else 
				$select.append('<option value="'+ formats[i] +'">' + formats[i] + '</option>');
		}
		$('.type-select').chosen({});

		//this list is not complete.
		this.formatIcons["Book"] = "11a";
		this.formatIcons["Journal / Magazine"] = "12a";
		this.formatIcons["Film or Video"] = "02a";
		this.formatIcons["Map"] = "01a";
		this.formatIcons["Sound Recording"] = "03a";
		this.formatIcons["Music Recording"] = "04a";
		this.formatIcons["3D Object"] = "09a";
		this.formatIcons["DVD"] = "02a";
		this.formatIcons["eBook"] = "11b";
		this.formatIcons["Software / Computer File"] = "06a";

		if(that.query) {
        	var searchField = $("#search-field")[0];
			searchField.value = that.query;

			//setting the values of the selectors
			$("#type-selector").val(that.type).trigger('chosen:updated');
			$("#format-selector").val(that.format).trigger('chosen:updated');
		}
		$("#drip").text("+");
	},
	clickListener: function(obj) {
		this.query = $("#search-field").val();
		var type = $("#type-selector").chosen().val();
		var format = $("#format-selector").chosen().val();
		
		//This is supposed to get the call number type from the radio buttons it is not working
		callNumbertype = $("input:radio[name=classification]:checked").val();
		if(callNumbertype == undefined )
		{
			callNumbertype = "";
		}
		console.log(callNumbertype);
		/*$('#bugs input').on('change', function() {
   		callNumbertype = $('table[id$=bugs] input:radio:checked').val();
		});
		console.log(callNumbertype);*/
		
		this.page = 1;
 		if ($("#drip").text() == "-") { $("#bugs").slideToggle("slow"); $("#drip").text("+"); }
		//console.log(type + " " + format);
		this.doQuery(this.query, type, format);
		console.log("loc/"+chooseBookColl.location+"/"+encodeURIComponent(this.query)+"+"+type+"+"+format+"+"+"1");
		router.navigate("loc/"+chooseBookColl.location+"/"+encodeURIComponent(this.query)+"+"+type+"+"+format+"+"+"1", {trigger: false});
		return false;
	},
	prevPage: function(e) {
		console.log("prev page link was clicked");
		this.page--;
		this.doQuery();
		router.navigate("loc/"+chooseBookColl.location+"/"+encodeURIComponent(this.query)+"+"+this.type+"+"+this.format+"+"+this.page, {trigger: false});
		e.preventDefault();
	},
	nextPage: function(e) {
		console.log("next page link was clicked");
		this.page++;
		this.doQuery();
		router.navigate("loc/"+chooseBookColl.location+"/"+encodeURIComponent(this.query)+"+"+this.type+"+"+this.format+"+"+this.page, {trigger: false});
		e.preventDefault();
        //router.navigate("http://google.com", {trigger: false});

	},
	doQuery: function(q, type, format)
	{
		that = this;
		if(q) {
			this.query = q;
			this.type = type;
			this.format = format;
			console.log("the values have been set.");
		}
		var options = {
			data: {
				loc: chooseBookColl.location,
				query: that.query,
				type: that.type,
				page: that.page,
				format: that.format,
				//callNumbertype: that.callNumbertype
			},
			success: function(res) {
				var $select = $("#books-listing");
				$select.empty();
				var bookT = _.unescape($(that.fullTemplate).find('.item')[0].outerHTML);
				res.each(function(item) {
					var data = {
						title: item.get("title"), 
						bibId: item.get("bibId"),
						imgUrl: item.get("thumbnail"), 
						author: item.get("author"),
pubYear: item.get("pubYear"),
						location: item.get("location"),
						format: item.get("format"), 
						formatUrl: that.formatIcons[item.get("format")]
					};
					var compiledT = _.template(bookT, {data:data});
					$select.append(compiledT);
				});
				console.log(res.length);

				if(res.length>10) {
					$('.pager').show();
				} else {
					$('.pager').hide();
				}
				if(res.length==0) {
					$('#nothing').show();
				} else {
					$('#nothing').hide();
				}

				$(".thumbnail").each(function(index) {
					this.onload = function() {
						if(this.naturalWidth == 1 || this.naturalWidth == 0) {
							this.src = "./no_photo.jpg";
						}
					}
				});
			},
			error: function(e) {
				console.log("hmm, looks like there was some error");
			}
		};
		chooseBookColl.fetch(options);
		$("html, body").animate({ scrollTop: 0 }, "slow");
	},
	bookClick: function(obj) {
		//console.log("the val: " + $(obj.target).parent().parent().attr("value"));
        //router.navigate("http://google.com", {trigger: true});

		router.navigate("loc/"+chooseBookColl.location+"/"+encodeURIComponent(this.query)+"/"+$(obj.target).closest(".item").attr("value"), {trigger: true});
	},
	dropdown: function() {
   	 $("#bugs").slideToggle();
   	 if ($("#drip").text() == "-") { $("#drip").text("+"); setTimeout(function (){

             $("input:radio").removeAttr("checked");

         }, 450) } else { $("#drip").text("-"); }
   	 /*
   	 $("#drip").animate({ top:"-=50px", left:"+=40px"}, "slow");
   	 $("#drip").animate({ top:"-=50px", left:"-=10px"}, "slow");
   	 $("#drip").animate({ left:"-=40px", top:"+=150px"});
   	 $("#drip").animate({ left:"+=90px", top:"+=50px"}, "slow");
   	 $("#drip").animate({ left:"-=120px", top:"-=260px"});
   	 $("#drip").animate({ left:"+=40px", top:"+=160px"});

   	 $("#search").animate({ left:"+=40px", top:"+=160px"});
   	 $("#search").animate({ left:"-=120px", top:"-=260px"}, "slow");
   	 $("#search").animate({ left:"+=90px", top:"+=50px"});
   	 $("#search").animate({ left:"-=40px", top:"+=150px"});
   	 $("#search").animate({ top:"-=50px", left:"-=10px"}, "slow");
   	 $("#search").animate({ top:"-=50px", left:"+=40px"});

   	 $("#bugs").animate({ left:"-=120px", top:"-=260px"});
   	 $("#bugs").animate({ left:"+=40px", top:"+=160px"}, "slow");
   	 $("#bugs").animate({ left:"+=90px", top:"+=50px"});
   	 $("#bugs").animate({ left:"-=10px", top:"+=40px"});   	 
   	 $("#bugs").animate({ left:"-=40px", top:"+=20px"});
   	 $("#bugs").animate({ left:"+=40px", top:"-=10px"}, "slow");
*/
   	 /*$("#hidden-cat").show();

   	 if ($("#drip").text() == "+") { $("#hidden-cat").animate({ top:"+=254px"}, 2); }
   	 else { $("#hidden-cat").animate({ top:"-=254px"}, 2); }
   	 $("#hidden-cat").animate({ left:"+=500px", top:"-=4500px"}, 2);
   	 $("#hidden-cat").animate({ left:"-=1500px"}, 2);
   	 $("#hidden-cat").animate({ top:"+=1004px"}, 2);
   	 $("#hidden-cat").animate({ left:"+=2500px"}, 38000);
   	 
   	 $("#hidden-cat").animate({ left:"-=500px", top:"+=4500px"}, 2);
   	 $("#hidden-cat").animate({ left:"+=1500px"}, 2);
   	 $("#hidden-cat").animate({ top:"-=1004px"}, 2);
   	 $("#hidden-cat").animate({ left:"-=2500px"}, 2);*/
/*
   	 $("#show-btn").animate({ left:"-=120px", top:"-=200px"});
   	 $("#show-btn").animate({ left:"+=20px", top:"+=90px"});
   	 $("#show-btn").animate({ left:"+=45px", top:"-=200px"}, "slow");
   	 $("#show-btn").animate({ left:"+=200px", top:"+=310px"});
   	 $("#show-btn").animate({ left:"-=85px", top:"-=70px"}, "slow");
   	 $("#show-btn").animate({ left:"-=60px", top:"+=70px"});    
   	 */
    }
});